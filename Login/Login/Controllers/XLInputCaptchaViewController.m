//
//  XLInputCaptchaViewController.m
//  xinLing
//
//  Created by 于鹏 on 2017/7/26.
//  Copyright © 2017年 YIVIEW. All rights reserved.
//

#import "XLInputCaptchaViewController.h"
#import "XLCodeInputView.h"
#import "XLChangePwdViewController.h"
#import "CountDown.h"
#import "XLAppMacro.h"
//#import <BaseModule/CountDown.h>
//#import <XLBaseModule/XLAppMacro.h>

@interface XLInputCaptchaViewController ()<XLCodeInputDelegate>
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@property (weak, nonatomic) IBOutlet UIView *fatherView;
@property (weak, nonatomic) IBOutlet XLCodeInputView *codeInputView;
@property (weak, nonatomic) IBOutlet UIButton *getCodeBtn;
/** 倒计时*/
@property (nonatomic, strong) CountDown *countDown;

@end

@implementation XLInputCaptchaViewController
- (void)dealloc {
    XLLog(@"%@ delloc",[self class]);
    [_countDown destoryTimer];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"输入验证码";
//    //手动调用倒计时
//    if ([self respondsToSelector:@selector(getCodeAction:)]) {
//        [self getCodeAction:self.getCodeBtn];
//    }
    [self buttonCountDown];
    
}



- (void)initInterface {
//    self.phoneNum = @"12312312312";
    NSString *phoneNum = [self.phoneNum becomePhoneNumType];
    NSMutableString *text = [NSMutableString string];
    [text appendString:@"验证码已发送至手机号: "];
    [text appendFormat:@"%@,",phoneNum];
    [text appendString:@"\n请输入验证码"];
    self.textLabel.text = text.copy;
    self.textLabel.textColor = kXLTextColor_WHITE;
    [self.codeInputView beginEdit];
    self.codeInputView.delegate = self;
    [self.getCodeBtn setBackgroundColor:kRGB(18, 105, 177)];

}

#pragma mark - XLCodeInputDelegate

- (void)codeInputView:(XLCodeInputView *)codeInputView finished:(BOOL)finished {
    if (finished) {
        [self showWithStatus:@"正在验证"];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self pushToVC];
        });
//        [self checkSMS];
    }
}

#pragma mark - 验证验证码
- (void)checkSMS {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:self.phoneNum forKey:@"mobile"];
    [parameters setValue:[self.codeInputView getCode] forKey:@"mobileCode"];
    [parameters setValue:@"1" forKey:@"smsTypeId"];
    [XLHTTPRequest UM_checkSMSWithParameters:parameters success:^(id responseObject) {
        [self showSuccessWithStatus:@"验证成功"];
        [self pushToVC];
        
    } failure:^(NSError *error, NSString *info) {
        if (error == nil) {
            [self exitLogin:info];
        }
    }];
}
- (void)pushToVC {
    XLChangePwdViewController *changePwd = [[XLChangePwdViewController alloc]init];
    changePwd.phoneNum = self.phoneNum;
    [self.navigationController pushViewController:changePwd animated:YES];
}
#pragma mark - Action
- (IBAction)getCodeAction:(id)sender {
    [self getCodeRequest];
}

#pragma mark - 获取验证码
- (void)getCodeRequest {
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setValue:self.phoneNum forKey:@"mobile"];
        [parameters setValue:@"1" forKey:@"smsTypeId"];
    
        [XLHTTPRequest UM_getSMSWithParameters:parameters success:^(id responseObject) {
            [self showSuccessWithStatus:@"验证码已发送"];
            [self buttonCountDown];
        } failure:^(NSError *error, NSString *info) {
            if (error == nil) {
                [self exitLogin:info];
            }
        }];
}

#pragma mark - 倒计时
- (void)buttonCountDown {
    XLWeakSelf(self);
    _countDown = [[CountDown alloc]init];
    [_countDown countDownWithTime:60 completeBlock:^(NSInteger countDown) {
        if (countDown < 1) {
            [weakself.getCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
            [weakself.getCodeBtn setBackgroundColor:kRGB(18, 105, 177)];
            weakself.getCodeBtn.enabled = YES;
        } else {
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:1];
            [weakself.getCodeBtn setTitle:[NSString stringWithFormat:@"%lds后重新获取",countDown] forState:UIControlStateNormal];
            [UIView commitAnimations];
            [weakself.getCodeBtn setBackgroundColor:kRGB(133, 133, 133)];
            weakself.getCodeBtn.enabled = NO;
        }
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
