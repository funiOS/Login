
Pod::Spec.new do |s|
  s.name         = "Login"
  s.version      = "0.1.5"
  s.summary      = "Login."
  s.homepage     = "https://gitee.com/xuanxiuiOS/Login"
  s.license      = { :type => "MIT", :file => "FILE_LICENSE" }
  s.author             = { "fun." => "yupeng_ios@163.com" }
  s.platform     = :ios, "8.0"
  s.source       = { :git => "https://gitee.com/xuanxiuiOS/Login.git", :tag => s.version.to_s }
  s.source_files  = "Login/Login/**/*.{h,m}"
  s.resources = 'Login/Login/Resources/*.{png,xib}'
  
  s.dependency 'XLBaseModule'
  s.dependency 'BaseModule/Utils'
  s.dependency 'IQKeyboardManager', '~> 4.0.9'
  s.dependency 'YYText'
  s.requires_arc = true
end
