//
//  XLCodeInputView.m
//  xinLing
//
//  Created by 于鹏 on 2017/7/26.
//  Copyright © 2017年 YIVIEW. All rights reserved.
//

#import "XLCodeInputView.h"
//#import <Masonry.h>
//#import <XLBaseModule/XLAppMacro.h>
#import "Masonry.h"
#import "XLAppMacro.h"
#define MaxLength 4
#define kTag 1000

@interface XLCodeInputView ()<UITextFieldDelegate>

@property (nonatomic, strong)UITextField *textField;
@property (nonatomic, strong)UIView *containerView;

@end

@implementation XLCodeInputView

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initUI];
}

- (instancetype)init {
    if (self = [super init]) {
        [self initUI];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self initUI];
    }
    return self;
}
- (void)initUI {
    self.backgroundColor = [UIColor clearColor];
    [self addSubview:self.textField];
    self.textField.delegate = self;
    [self.textField addTarget:self action:@selector(textDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self addSubview:self.containerView];
    [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self).with.insets(UIEdgeInsetsZero);
    }];
    CGFloat margin = 10;
    CGFloat leftX = 30;
//    CGFloat width = (kInputViewWidth - (MaxLength-1)*margin)/MaxLength;
    CGFloat width = (KScreenWidth - (MaxLength-1)*margin - 2*leftX)/MaxLength;

//    XLLog(@"%f",width);
    for (int i = 0; i<MaxLength; i++) {
       
        UILabel *label = [[UILabel alloc]init];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = XLFONT(30);
        label.textColor = XLForgetPwd_TextFieldTextColor;
        [self.containerView addSubview:label];
//        label.text = @"2";
        label.tag = kTag+i;
        
        CGFloat leftMargin = (width+margin)*i + leftX;
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.containerView.mas_top).offset(0);
            make.left.equalTo(self.containerView.mas_left).offset(leftMargin);
            make.width.mas_equalTo(width);
            make.bottom.equalTo(self.containerView.mas_bottom).offset(0);
        }];
        

        //线
        UIView *underline = [[UIView alloc]init];
        underline.backgroundColor = XLForgetPwd_UnderlineColor;
        [self.containerView addSubview:underline];
        
        [underline mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.containerView.mas_left).offset(leftMargin);
            make.width.mas_equalTo(width);
            make.height.mas_equalTo(1);
            make.bottom.equalTo(self.containerView.mas_bottom).offset(0);
        }];
        
    }
}
#pragma mark - UITextFieldDelegate
#pragma mark - Action 
- (void)textDidChange:(UITextField *)textField {
    NSString *str = textField.text;
    XLLog(@"textDidChange:%@",str);
    if (str.length >= MaxLength) { //如果>MaxLength,反向赋值给textView，这样删除，才能立即删除.
        str = [str substringToIndex:MaxLength];
        textField.text = str;
    }else { //不够补空格
        NSInteger needSpaceCount = MaxLength - str.length;
        if (needSpaceCount > 0) {
            NSMutableString *spaceStr = [NSMutableString string];
            for (int i = 0; i< needSpaceCount; i++) {
                [spaceStr appendString:@" "];
            }
            str = [NSString stringWithFormat:@"%@%@",str,spaceStr];
        }
    }
    
    //赋值
    for (int i = 0; i<MaxLength; i++) {
        NSString *temp = [str substringWithRange:NSMakeRange(i, 1)];
        UILabel *label = [self.containerView viewWithTag:kTag+i];
        label.text = temp;
        
        if (i == MaxLength-1) {
            if (self.delegate && [self.delegate respondsToSelector:@selector(codeInputView:finished:)]) {
                [self.delegate codeInputView:self finished:![temp isEqualToString:@" "]];
            }
        }
        
    }
    
    
}

#pragma mark - Public Method 
- (void)beginEdit {
    [self.textField becomeFirstResponder];
}

- (void)endEdit {
    [self.textField resignFirstResponder];
}

- (NSString *)getCode {
    return self.textField.text;
}

- (void)clearCode {
    self.textField.text = @"";
}
#pragma mark - Getter 
- (UITextField *)textField {
    if (!_textField) {
        _textField = [[UITextField alloc]init];
        _textField.hidden = YES;
        _textField.keyboardType = UIKeyboardTypeNumberPad;
    }
    return _textField;
}
- (UIView *)containerView {
    if (!_containerView) {
        _containerView = [[UIView alloc]init];
        
    }
    return _containerView;
}

@end
