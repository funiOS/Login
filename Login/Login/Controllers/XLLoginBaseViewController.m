//
//  XLLoginBaseViewController.m
//  xinLing
//
//  Created by 于鹏 on 2017/7/25.
//  Copyright © 2017年 YIVIEW. All rights reserved.
//

#import "XLLoginBaseViewController.h"
//#import <Masonry.h>
//#import <BaseModule/UIViewController+DismissKeyboard.h>
#import "Masonry.h"
#import "UIViewController+DismissKeyboard.h"

@interface XLLoginBaseViewController ()

@end

@implementation XLLoginBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self addBackgroundImage];
    
}

- (void)initData {
    
}

#pragma mark - 在view底部添加背景图
- (void)addBackgroundImage {
    UIImageView *bgImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"loginBg.png"]];
//    [self.view insertSubview:bgImgView atIndex:0];
    [self.view addSubview:bgImgView];
    [self.view sendSubviewToBack:bgImgView];
//    bgImgView.frame = self.view.frame;
    [bgImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view).insets(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

#pragma mark - 点击任何地方 收起键盘
- (void)dismissKeyboard {
    [self setupForDismissKeyboard];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
