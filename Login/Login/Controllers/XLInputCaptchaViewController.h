//
//  XLInputCaptchaViewController.h
//  xinLing
//
//  Created by 于鹏 on 2017/7/26.
//  Copyright © 2017年 YIVIEW. All rights reserved.
//

#import "XLLoginBaseViewController.h"
/** 输入验证码*/
@interface XLInputCaptchaViewController : XLLoginBaseViewController
/** 手机号*/
@property (nonatomic, copy) NSString *phoneNum;

@end
