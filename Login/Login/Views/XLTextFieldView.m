//
//  XLTextFieldView.m
//  Login
//
//  Created by 于鹏 on 2017/8/11.
//  Copyright © 2017年 YIVIEW. All rights reserved.
//

#import "XLTextFieldView.h"
//#import <Masonry.h>
//#import <XLBaseModule/XLAppMacro.h>
#import "Masonry.h"
#import "XLAppMacro.h"
@interface XLTextFieldView()

@property (nonatomic,strong)UITextField *textField;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UIView *underlineView;
@end

@implementation XLTextFieldView
@synthesize textFieldText = _textFieldText;
- (void)awakeFromNib {
    [super awakeFromNib];
    [self configUI];
}
- (instancetype)init {
    self = [super init];
    if (self) {
        [self configUI];
    }
    return self;
}

//- (void)configUI {
//    self.backgroundColor = [UIColor yellowColor];
//    [self addSubview:self.titleLabel];
//    [self addSubview:self.textField];
//    [self addSubview:self.underlineView];
//
//    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self).offset(0);
//        make.bottom.equalTo(self.mas_bottom).offset(-5);
//        make.width.mas_equalTo(0);
//    }];
//
//    CGFloat leftX = 0;
//    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.leading.equalTo(self.titleLabel.mas_trailing).offset(leftX);
//        make.bottom.equalTo(self.mas_bottom).offset(-5);
//        make.right.equalTo(self.mas_right).offset(0);
//    }];
//
//    [self.underlineView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.titleLabel.mas_right).offset(leftX);
//        make.bottom.equalTo(self.mas_bottom).offset(0);
//        make.right.equalTo(self.mas_right).offset(0);
//        make.height.mas_equalTo(1);
//    }];
//    [self configTextField];
//}

- (void)configUI {
    self.backgroundColor = [UIColor yellowColor];
    [self addSubview:self.titleLabel];
    [self addSubview:self.textField];
    [self addSubview:self.underlineView];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(0);
        make.bottom.equalTo(self.mas_bottom).offset(-5);
        make.top.equalTo(self.mas_top).offset(5);
        make.width.mas_equalTo(0);
    }];
    
    CGFloat leftX = 0;
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.titleLabel.mas_trailing).offset(leftX);
        make.bottom.equalTo(self.mas_bottom).offset(-5);
        make.right.equalTo(self.mas_right).offset(0);
        make.top.equalTo(self.mas_top).offset(5);
    }];
    
    [self.underlineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLabel.mas_right).offset(leftX);
        make.bottom.equalTo(self.mas_bottom).offset(0);
        make.right.equalTo(self.mas_right).offset(0);
        make.height.mas_equalTo(1);
    }];
    [self configTextField];
}

- (void)configTextField {
    self.backgroundColor = [UIColor clearColor];
    self.textField.tintColor = [UIColor whiteColor];
//    [self.textField setValue:UIColorFromRGBA(0xffffff, .6f) forKeyPath:@"_placeholderLabel.textColor"];
    UIButton *pwdClearButton = [self.textField valueForKey:@"_clearButton"];
    [pwdClearButton setImage:[UIImage imageNamed:@"login_icon_clear"] forState:UIControlStateNormal];
}
#pragma mark - Setter

- (void)setPlaceHolderText:(NSString *)placeHolderText {
    _placeHolderText = placeHolderText;
    self.textField.attributedPlaceholder = [[NSAttributedString alloc]initWithString:placeHolderText attributes:@{NSForegroundColorAttributeName:UIColorFromRGBA(0xffffff, .6f)}];
}

- (void)setTitle:(NSString *)title {
    _title = title;
    self.titleLabel.text = title;
//    CGSize titleSize = [title sizeWithAttributes:@{NSFontAttributeName:self.titleLabel.font}];
    CGSize titleSize = [self.placeholderTitle.length > 0 ? self.placeholderTitle : title sizeWithAttributes:@{NSFontAttributeName:self.titleLabel.font}];

    //更新titleLabel约束 +5:距离textField的距离
    [self.titleLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(titleSize.width+5);
    }];
}

- (void)setHideUnderline:(BOOL)hideUnderline {
    _hideUnderline = hideUnderline;
    self.underlineView.hidden = hideUnderline;
}

- (void)setKeyboardType:(UIKeyboardType)keyboardType {
    _keyboardType = keyboardType;
    self.textField.keyboardType = keyboardType;
}

- (void)setSecureTextEntry:(BOOL)secureTextEntry {
    _secureTextEntry = secureTextEntry;
    self.textField.secureTextEntry = secureTextEntry;
}

- (void)setTextFieldText:(NSString *)textFieldText {
    _textFieldText = textFieldText;
    self.textField.text = textFieldText;
}

#pragma mark - Getter

- (NSString *)textFieldText {
    return self.textField.text;
}
- (BOOL)isNotEmptyString {
    return self.textField.text.length > 0 ? YES : NO;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.font = XLFONT(15);
//        _titleLabel.textAlignment = NSTextAlignmentLeft;
        _titleLabel.textAlignment = NSTextAlignmentCenter;

    }
    return _titleLabel;
}
- (UITextField *)textField {
    if (!_textField) {
        _textField = [[UITextField alloc]init];
        _textField.textColor = [UIColor whiteColor];
        _textField.textAlignment = NSTextAlignmentLeft;
        _textField.borderStyle = UITextBorderStyleNone;
        [_textField setClearButtonMode:UITextFieldViewModeWhileEditing];
        _textField.font = XLFONT(15);
        _textField.placeholder = @"";
    }
    return _textField;
}

- (UIView *)underlineView {
    if (!_underlineView) {
        _underlineView = [[UIView alloc]init];
        _underlineView.backgroundColor = [UIColor lightTextColor];
    }
    return _underlineView;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
