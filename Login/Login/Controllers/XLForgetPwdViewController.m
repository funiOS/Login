//
//  XLForgetPwdViewController.m
//  xinLing
//
//  Created by 于鹏 on 2017/7/25.
//  Copyright © 2017年 YIVIEW. All rights reserved.
//

#import "XLForgetPwdViewController.h"
#import "XLInputCaptchaViewController.h"

@interface XLForgetPwdViewController ()

@property (weak, nonatomic) IBOutlet XLEditedButton *nextBtn;

@property (weak, nonatomic) IBOutlet XLTextFieldView *phoneTextField;

@end

@implementation XLForgetPwdViewController
- (void)dealloc {
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(textFieldDidChanged:) name:UITextFieldTextDidChangeNotification object:nil];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"输入账户";
    self.phoneTextField.placeHolderText = @"请输入11位中国大陆手机号";
    self.phoneTextField.keyboardType = UIKeyboardTypeNumberPad;
    self.phoneTextField.title = @"+86";
    self.nextBtn.enabled = NO;
    [self dismissKeyboard];
    
}

#pragma mark - Notification

- (void)textFieldDidChanged:(NSNotification *)notification {
    if (self.phoneTextField.isNotEmptyString) {
        self.nextBtn.enabled = YES;
    } else {
        self.nextBtn.enabled = NO;
    }
}
#pragma mark - Action
//- (IBAction)nextBtnAction:(id)sender {
////    BOOL isVaild = [self.phoneTextField.textFieldText jk_isMobileNumber];
//    XLInputCaptchaViewController *vc = [[XLInputCaptchaViewController alloc]init];
//    vc.phoneNum = self.phoneTextField.textFieldText;
//    [self.navigationController pushViewController:vc animated:YES];
//}
- (IBAction)nextBtnAction:(id)sender {
    BOOL isVaild = [self.phoneTextField.textFieldText jk_isMobileNumber];
    if (!isVaild) {
        [self showErrorWithStatus:@"手机号格式不正确!"];
        return;
    }
    XLLog(@"手机号有效");
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:self.phoneTextField.textFieldText forKey:@"mobile"];
    [parameters setValue:@"1" forKey:@"smsTypeId"];

    [XLHTTPRequest UM_getSMSWithParameters:parameters success:^(id responseObject) {
            XLInputCaptchaViewController *vc = [[XLInputCaptchaViewController alloc]init];
            vc.phoneNum = self.phoneTextField.textFieldText;
            [self.navigationController pushViewController:vc animated:YES];
    } failure:^(NSError *error, NSString *info) {
        if (error == nil) {
            [self exitLogin:info];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
