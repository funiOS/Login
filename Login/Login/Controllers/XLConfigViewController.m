//
//  XLConfigViewController.m
//  Login
//
//  Created by 于鹏 on 2017/10/23.
//  Copyright © 2017年 YIVIEW. All rights reserved.
//

#import "XLConfigViewController.h"

@interface XLConfigViewController ()
@property (weak, nonatomic) IBOutlet XLTextFieldView *addressTextField;
@property (weak, nonatomic) IBOutlet XLTextFieldView *portTextField;
@property (weak, nonatomic) IBOutlet XLTextFieldView *tcpPortTextField;
@property (weak, nonatomic) IBOutlet XLEditedButton *submitButton;

@end

@implementation XLConfigViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(textFieldDidChange:) name:UITextFieldTextDidChangeNotification object:nil];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"服务器配置";
    NSString *placeholderTitle = @"TCP端口号";
    self.addressTextField.placeholderTitle = placeholderTitle;
    self.portTextField.placeholderTitle = placeholderTitle;
    self.tcpPortTextField.placeholderTitle = placeholderTitle;
    
    self.addressTextField.placeHolderText = @"请输入服务器地址";
    self.addressTextField.title = @"IP地址";
    self.portTextField.placeHolderText = @"请输入端口号";
    self.portTextField.title = @"端口号";
    self.tcpPortTextField.placeHolderText = @"请输入TCP端口号";
    self.tcpPortTextField.title = placeholderTitle;
    
    self.portTextField.keyboardType = UIKeyboardTypeNumberPad;
    self.tcpPortTextField.keyboardType = UIKeyboardTypeNumberPad;
    [self judgeContentValid];

    
    
    self.addressTextField.textFieldText = UserInfo.serviceInfo.serviceAddress;
    self.portTextField.textFieldText = UserInfo.serviceInfo.port;
    self.tcpPortTextField.textFieldText = UserInfo.serviceInfo.tcpPort;
  
}



- (IBAction)submitAction:(id)sender {
    if (![self judgeContentValid]) {
        return;
    }
    [self showWithStatus:@"正在提交"];
    
    XLServiceInfo *serviceInfo = [[XLServiceInfo alloc]initWithServiceAddress:self.addressTextField.textFieldText port:self.portTextField.textFieldText tcpPort:self.tcpPortTextField.textFieldText];
    UserInfo.serviceInfo = serviceInfo;
    [UserInfo synchronize];
    
    [self showSuccessWithStatus:@"提交成功"];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController popViewControllerAnimated:YES];
    });
}

- (void)textFieldDidChange:(NSNotification *)notification {
    [self judgeContentValid];
}

- (BOOL)judgeContentValid {
    if ([self.addressTextField isNotEmptyString] && [self.addressTextField isNotEmptyString] && [self.tcpPortTextField isNotEmptyString]) {
        self.submitButton.enabled = YES;
        return YES;
    } else {
        self.submitButton.enabled = NO;
        return NO;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
