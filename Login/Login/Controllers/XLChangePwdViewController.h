//
//  XLChangePwdViewController.h
//  xinLing
//
//  Created by 于鹏 on 2017/7/27.
//  Copyright © 2017年 YIVIEW. All rights reserved.
//

#import "XLLoginBaseViewController.h"
/** 重置密码*/
@interface XLChangePwdViewController : XLLoginBaseViewController
/** 手机号*/
@property (nonatomic, copy) NSString *phoneNum;

@end
