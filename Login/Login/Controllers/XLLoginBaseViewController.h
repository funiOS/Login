//
//  XLLoginBaseViewController.h
//  xinLing
//
//  Created by 于鹏 on 2017/7/25.
//  Copyright © 2017年 YIVIEW. All rights reserved.
//


//#import <XLBaseModule/XLBaseViewController.h>
#import "XLBaseViewController.h"
#import "XLEditedButton.h"
#import "XLTextFieldView.h"
#import "NSString+JKNormalRegex.h"


@interface XLLoginBaseViewController : XLBaseViewController
#pragma mark - 点击任何地方 收起键盘
- (void)dismissKeyboard;
@end
