//
//  XLEditedButton.m
//  xinLing
//
//  Created by 于鹏 on 2017/7/25.
//  Copyright © 2017年 YIVIEW. All rights reserved.
//

#import "XLEditedButton.h"
//#import <BaseModule/UIImage+YPColor.h>
//#import <XLBaseModule/XLAppMacro.h>
#import "UIImage+YPColor.h"
#import "XLAppMacro.h"
@implementation XLEditedButton

- (void)awakeFromNib {
    [super awakeFromNib];
    [self configUI];
}
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self configUI];
    }
    return self;
}

- (void)configUI {

    [self setBackgroundImage:[UIImage yp_imageWithColor:kRGB(18, 105, 177)] forState:UIControlStateNormal];
    [self setTitleColor:kXLTextColor_WHITE forState:UIControlStateNormal];
    self.titleLabel.font = XLFONT(15);
//    self.layer.cornerRadius = 25;
    self.layer.cornerRadius = CGRectGetHeight(self.frame)/2;
    self.layer.masksToBounds = YES;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
