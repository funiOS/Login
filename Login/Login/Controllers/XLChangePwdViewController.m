//
//  XLChangePwdViewController.m
//  xinLing
//
//  Created by 于鹏 on 2017/7/27.
//  Copyright © 2017年 YIVIEW. All rights reserved.
//

#import "XLChangePwdViewController.h"

@interface XLChangePwdViewController ()
/** 新密码*/
@property (weak, nonatomic) IBOutlet XLTextFieldView *pwdTF;
/** 确认密码*/
@property (weak, nonatomic) IBOutlet XLTextFieldView *confirmPwdTF;
@property (weak, nonatomic) IBOutlet XLEditedButton *sureButton;

@end

@implementation XLChangePwdViewController

- (void)dealloc {
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(textFieldDidChanged:) name:UITextFieldTextDidChangeNotification object:nil];

    self.title = @"重置密码";
    [self dismissKeyboard];
    
    [self removeSecondVC];
}

/**
 *  从当前导航控制器中删除第二个页面，可以从第二页返回到RootVC
 */
- (void)removeSecondVC
{
    //得到当前视图控制器中的所有控制器
    NSMutableArray *array = [self.navigationController.viewControllers mutableCopy];
    //把输入验证码界面从里面删除
    [array removeObjectAtIndex:2];
    //把删除后的控制器数组再次赋值
    [self.navigationController setViewControllers:[array copy] animated:YES];
}

- (void)initInterface {
    _pwdTF.title = @"    新密码:";
    _confirmPwdTF.title = @"确认密码:";
    self.sureButton.enabled = NO;
}

#pragma mark - Notification

- (void)textFieldDidChanged:(NSNotification *)notification {
    if (self.pwdTF.isNotEmptyString && self.confirmPwdTF.isNotEmptyString /*&& [self.confirmPwdTF.textFieldText isEqualToString:self.pwdTF.textFieldText]*/ ) {
        self.sureButton.enabled = YES;
    } else {
        self.sureButton.enabled = NO;
    }
}

- (IBAction)sureBtnAction:(id)sender {
    if ([self.confirmPwdTF.textFieldText isEqualToString:self.pwdTF.textFieldText]) {
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setValue:self.pwdTF.textFieldText forKey:@"passWord"];
        [parameters setValue:self.phoneNum forKey:@"userName"];
        [XLHTTPRequest UM_resetPwdWithParameters:parameters success:^(id responseObject) {
            [self showSuccessWithStatus:@"重置密码成功"];
            [self.navigationController popToRootViewControllerAnimated:YES];
        } failure:^(NSError *error, NSString *info) {
            if (error == nil) {
                [self exitLogin:info];
            }
        }];
        
    } else {
        
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
