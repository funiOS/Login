//
//  XLTextFieldView.h
//  Login
//
//  Created by 于鹏 on 2017/8/11.
//  Copyright © 2017年 YIVIEW. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XLTextFieldView : UIView
/** placeholder text*/
@property (nonatomic, copy) NSString *placeHolderText;
/** textfield  text*/
//@property (nonatomic, copy, readonly) NSString *textFieldText;
@property (nonatomic, copy) NSString *textFieldText;

/** title*/
@property (nonatomic, copy) NSString *title;

/** 占位title,用于多个输入框,title长度不同时保证长度统一,在title前赋值*/
@property (nonatomic, copy) NSString *placeholderTitle;

/** 键盘样式*/
@property (nonatomic, assign)UIKeyboardType keyboardType;

/** 密码输入形式*/
@property (nonatomic, assign) BOOL secureTextEntry;       // default is NO

/** textField.text 是否为空*/
@property (nonatomic, assign, readonly)BOOL isNotEmptyString;

/** 隐藏底部的线*/
@property (nonatomic, assign, getter=isHideUnderline)BOOL hideUnderline;

@end
