//
//  XLCodeInputView.h
//  xinLing
//
//  Created by 于鹏 on 2017/7/26.
//  Copyright © 2017年 YIVIEW. All rights reserved.
//

#import <UIKit/UIKit.h>

@class XLCodeInputView;

@protocol XLCodeInputDelegate <NSObject>

@optional

- (void)codeInputView:(XLCodeInputView *)codeInputView finished:(BOOL)finished;

@end

@interface XLCodeInputView : UIView

@property (weak, nonatomic) id<XLCodeInputDelegate> delegate;

- (void)beginEdit;

- (void)endEdit;

- (NSString *)getCode;

- (void)clearCode;

@end
